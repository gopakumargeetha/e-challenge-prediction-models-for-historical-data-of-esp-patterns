# Final Results

| Team | Place | Score |
| ------ | ------ |------ |
| VESTA  | 1st | -0.783 |
| ACS Machine Learning | 2nd | -0.852 |
| MARS | 3rd | -2.144 |

# Tutorials
- Winners & Solution:[https://www.youtube.com/watch?v=uCO0CiKhTDQ](https://www.youtube.com/watch?v=uCO0CiKhTDQ). 
- Data Structuring:[https://www.youtube.com/watch?v=BPrIk78gfHg&t](https://www.youtube.com/watch?v=BPrIk78gfHg&t). 
- Submission Tutorial:[https://www.youtube.com/watch?v=x556-i2rdF8&t=147s](https://www.youtube.com/watch?v=x556-i2rdF8&t). 
- Conferences & Speakers:[https://www.spe-ecuador.org/e-challenge-machine-learning-version](https://www.spe-ecuador.org/e-challenge-machine-learning-version).

# E-challenge - Prediction Models for Historical Data of ESP Patterns

This project contains all the material, data, code an solutions of the E-challenge Machine Learning Contest organized by the SPE Ecuador Section on 2021, it is released under CC BY-NC-SA 4.0 license, only for academic purposes. 

Look the Competition on Codalab: [E-Challenge Codalab](https://competitions.codalab.org/competitions/34379).


## Description

Data directory contains two compressed files called Train and Test, each one contains High Frequency records of ESP Pumps from 2019 to 2021, also you will find historical production data about this field. The goal will be to predict the label ['WellFailure'] for testing data, i.e., you have to predict when an event ('yes' or 'Manual off') will occur based on your training.

 - 'yes' refers to a Failure event in the pump.

 - 'Manual off' refers to a Reconditioning event.

Remember, you are free to use any library and open-source framework written in Python for this challenge.

## Test and Deploy

All code submissions will be published in this repository after September 24th, 2021.

***

## Support
You can send your questions to the next address: [e-challenge@spe-ecuador.org](e-challenge@spe-ecuador.org)

## Roadmap
All the derived products of this repository must be shared with the same license.

## Contributing
For the model submission stage, only participants will be requested to share their code, after deadline on September 24th, 2021. Any person is welcome to contribute with this repository.

## Authors and acknowledgment
SPE Ecuador Section as organizer of the contest and Schlumberger as the main sponsor.
For more information refer to:[E-Challenge Website](https://www.spe-ecuador.org/e-challenge-machine-learning-version).

## License
CC BY-NC-SA 4.0.
-For more information refer to:  [Creative Commons License](https://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1) and
[E-Challenge Website](https://www.spe-ecuador.org/e-challenge-machine-learning-version) 

## Project status
Model Submissions

